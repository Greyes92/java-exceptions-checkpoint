package com.galvanize;

public class InvalidFormatException extends Exception{
    public InvalidFormatException (String msg){
        super(msg);
    }
}


package com.galvanize;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ZipCodeProcessorTest {

    private Verifier verifier;
    private ZipCodeProcessor zipCodeProcessor;

    @BeforeEach
    public void setUp() {
        verifier = new Verifier();
        zipCodeProcessor = new ZipCodeProcessor(verifier);
    }

    // write your tests here
    @Test
    public void thankYouMessageTest() {
        //arrange
        //act
        String actual = zipCodeProcessor.process("22345");
        String expected = "Thank you!  Your package will arrive soon.";
        //assert
        assertEquals(expected, actual);
    }

    //error message for length being too long
    @Test
    public void inputTooLongTest() {
        String actual = zipCodeProcessor.process("777777");
        String expected = "The zip code you entered was the wrong length.";

        assertEquals(expected, actual);
    }

    @Test
    public void inputTooShortTest() {
        String actual = zipCodeProcessor.process("7777");
        String expected = "The zip code you entered was the wrong length.";

        assertEquals(expected, actual);
    }

    @Test
    public void noServiceTest() {
        String actual = zipCodeProcessor.process("12345");
        String expected = "We're sorry, but the zip code you entered is out of our range.";

        assertEquals(expected, actual);
    }

}